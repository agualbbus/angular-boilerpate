angular.module('myApp.states.main')

.controller('MasterDetailController', ['$rootScope', '$location', '$scope', '$state', 'modelsCollection', function($rootScope, $location, $scope, $state, monsters) {
    

  $scope.masterDetailCtrl = {
    // waiting on ui-router to support angular 1.2 dynamic animations properly, for now everything is 'crossfade'
    // https://github.com/angular-ui/ui-router/issues/320

    animate : "crossfade",
    collection:modelsCollection
   }



 $scope.createModel = function() {
  
    $scope.masterDetailCtrl.collection.addModel().then(function() {
		
	  var created='';//create your function/service/whatever to find your created object and usit here;		
      alert('You created a new object'+created);
      

    });

  }

  $scope.destroyModel = function(item) {
    item.destroy().then(function() {
      alert('Destroyed model with destroy()')
      $state.transitionTo('');//pass the name of a X state if you want
    });
  }


}]);