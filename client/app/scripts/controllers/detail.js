angular.module('myApp.states.demo')

.controller('DetailController', ['$rootScope', '$scope', '$state', '$stateParams', function($rootScope, $scope, $state, $stateParams) {
  
  $scope.detailCtrl = {
    current : null
  };

  // inherit the collection from the parent controller and find the current model
  
   $scope.detailCtrl.current = $scope.masterDetailCtrl.collection.find( $stateParams.modelId );//you need to create your own find() function
      
   



}]);