angular.module('myApp.states.main',[])

.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {

	$stateProvider

	.state('main', {
		abstract: true,
        url: '',
        views: {
            '@': {
                templateUrl: 'app/views/app-layout.html',
            },
            'panel@main': {
                templateUrl: 'app/views/master-detail.html',
                controller: 'MasterDetailController',
                resolve: {
                    modelsCollection: function() {
                        //put here your function to collect models
                        return [
                        	{name:'joe',email:'joe@joe.com'},
                        	{name:'cris', email:'cris@cris.com'}
                        
                        ]	
   		
                    }
                }

            }
        }
    })


    .state('main.detail', {
    	url: 'crud/{modelId}',
    	views: {
    		
    		'detail@main' : {
    			templateUrl: 'app/views/detail/detail.html',
                controller: 'DetailController'
    		}

    	}
    })




}])