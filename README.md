# A Quickstart Boilerplate for Big AngularJS apps


An opinionated, battle-tested boilerplate for building large, powerful, serverless apps.

------
### Features
* AngularJS wrapper for the **Parse JS SDK**
* AngularJS wrapper for **Cloud Code**
* AngularJS wrapper for the **Facebook API**
* Bootstrap 2.3.2 + HTML5BP
* Enhanced Parse.Object and Parse.Collection with `load()`, `saveParse()`, `destroyParse()`
* State Manager using [ui-router](https://github.com/angular-ui/ui-router)
* Resolve Parse Data Before State Changes for awesome UX e.g. `return collection.load()`
* Easy Animations on State Change
* Enhanced Load Performance of the Parse and Facebook SDKs
* Data Modules for Parse Models and Collections
* [Genesis-Skeleton](https://github.com/ericclemmons/genesis-skeleton) node / grunt setup for local development
* …and lots more

------

### Installation Instructions

1. Install node - http://nodejs.org/
1. Install grunt v0.4.x - http://gruntjs.com/getting-started
1. Install bower `npm install -g bower`
1. Install coffee-script `npm install -g coffee-script`
1. Make sure you have compass installed (http://compass-style.org/install/)
1. Make sure you are running the latest version of Node (we can't assure you this is gonna work on older versions of Node)
1. Clone this repo `git https://bitbucket.org/agualbbus/angular-boilerpate`
1. `cd parse-angular-demo`
1. `npm install`
1. `grunt server` - builds and fires up the local node server on localhost:3000
1. Visit http://localhost:3000 to develop your site
1. `grunt prod` to build a deployable version in the parse-angular-demo/build folder (`grunt prod` is actually `build` followed by `optimize`)


------



### Credits
[brandid/parse-angular-demo](https://github.com/brandid/parse-angular-demo) - Based on this Repo